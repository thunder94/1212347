import React, {Component} from 'react';
import { AppRegistry, View } from 'react-native';
import AppRouter from './app/components/Router'
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor } from './app/store';

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <PersistGate persistor={persistor}>
                    <AppRouter/>
                </PersistGate>
            </Provider>
        );
    }
}
export default App

AppRegistry.registerComponent('foodDeliveryApp', ()=> App);
