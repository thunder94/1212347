// import libraries
import React from 'react';
import {Router, Scene, Stack, Tabs} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    StyleSheet,
    View
} from 'react-native';

// import components
import Register from '../components/authentication/Register';
import Login from '../components/authentication/Login';
import ForgetPassword from '../components/authentication/ForgetPassword';
import OrderHistory from './order/OrderHistory';
import OrderNavBar from './order/OrderNavBar';
import OrderDetail from './order/OrderDetail';
import Basket from './merchant/Basket';
import BasketNavBar from "./merchant/BasketNavBar";
import Account from './authentication/Account';
import Category from '../components/Category';
import Merchant from './merchant/Merchant';
import MerchantNavBar from './merchant/MerchantNavBar';
import Map from './merchant/Map';
import MapNavBar from './merchant/MapNavBar';
import MerchantDetail from './merchant/MerchantDetail';
import { string } from "../localization/i18n";

const App = () => (
    <Router>
        <Stack key="mainRoot">
            <Scene key="login"
                   component={Login}
                   hideNavBar={'true'}
            />
            <Scene key="register"
                   component={Register}
                   title={string('strSignUp')}
            />
            <Scene key="forget"
                   component={ForgetPassword}
                   title={string('strForget')}
            />
            <Scene key="mainTab"
                   tabs={true}
                   hideNavBar={'true'}
                   gesturesEnabled={'false'}
                   type={'replace'}
            >
                <Stack key="merchantRoot"
                       title={'Merchant'}
                       icon={({ focused }) => (
                           <Icon
                               size={20}
                               color={focused ? '#3399ff' : 'black'}
                               name={`building`}
                               textStyle={focused ? [styles.label, styles.activeLabel] : styles.label}
                           />
                       )}
                >
                    <Scene key={'merchant'}
                           component={Merchant}
                           navTransparent={1}
                           navBar={MerchantNavBar}
                           initial
                    />
                    <Scene key={'merchantDetail'}
                           component={MerchantDetail}
                    />
                    <Scene key={'map'}
                           component={Map}
                           navTransparent={1}
                           navBar={MapNavBar}
                    />
                </Stack>
                <Stack key="orderRoot"
                       title={'Order'}
                       icon={({ focused }) => (
                           <Icon
                               size={20}
                               color={focused ? '#3399ff' : 'black'}
                               name={`list`}
                               textStyle={focused ? [styles.label, styles.activeLabel] : styles.label}
                           />
                       )}
                >
                    <Scene key={'orderHistory'}
                           component={OrderHistory}
                           initial
                           navTransparent={1}
                           navBar={OrderNavBar}
                    />
                    <Scene key={'orderDetail'}
                           component={OrderDetail}
                    />
                </Stack>
                <Scene key={'basket'}
                       title={'Basket'}
                       component={Basket}
                       navTransparent={1}
                       navBar={BasketNavBar}
                       icon={({ focused }) => (
                           <Icon
                               size={20}
                               color={focused ? '#3399ff' : 'black'}
                               name={`shopping-cart`}
                               textStyle={focused ? [styles.label, styles.activeLabel] : styles.label}
                           />
                       )}
                />
                <Scene key={'account'}
                       title={'Account'}
                       component={Account}
                       hideNavBar={'true'}
                       icon={({ focused }) => (
                           <Icon
                               size={20}
                               color={focused ? '#3399ff' : 'black'}
                               name={`child`}
                               textStyle={focused ? [styles.label, styles.activeLabel] : styles.label}
                           />
                       )}
                />
            </Scene>
        </Stack>
    </Router>
);


export default App;

//define styles
const styles = StyleSheet.create({
    activeLabel: {
        color: '#3399ff'
    },
    label: {
        color: 'black'
    }
});
