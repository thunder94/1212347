// import libraries
import React, {Component} from 'react';
import {
    View,
    ScrollView,
    Text,
    TextInput,
    Image,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
    AsyncStorage, Alert
} from 'react-native';
import {Actions} from "react-native-router-flux";

export default class Category extends Component {
    constructor(props) {
        super(props);
        this.state = {
            content: [],
            categorySelected: 0
        };
    }
    // Save Category ID to store
    saveCategory =  async () => {
        try {
            await AsyncStorage.setItem('idCategory', this.state.categorySelected);
        }
        catch (error) {
            Alert.alert('Error', 'There was an error.')
        }
    };

    showMerchantList = (idCategory) => {
        this.setState ({
           categorySelected: idCategory
        });
        console.log(this.state.categorySelected);
        this.saveCategory();
        // To Merchant screen
        Actions.merchant();
    };


    componentWillMount = () => {
        fetch('https://food-delivery-server.herokuapp.com/categories/getAll', {
            method: 'GET'
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({content: responseJson});
                console.log(this.state.content);
            })
    };

    renderCategoryList() {
        return this.state.content.map(content => {
            return (
                <TouchableOpacity key={content.id} onPress={() => this.showMerchantList(content.id)}>
                    <ImageBackground key={content.id} style={styles.categoryImage} source={{uri: content.image}}>
                        <View key={content.id} style={styles.categoryContainer}>
                            <Text style={styles.lblCategory}>{content.name}</Text>
                        </View>
                    </ImageBackground>
                </TouchableOpacity>
            );
        })
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                {this.renderCategoryList()}
            </ScrollView>

        );
    }
}

// define styles

const styles = StyleSheet.create({
    container: {
        alignSelf: 'stretch',
        backgroundColor: 'white'
    },
        categoryImage: {
            height: 200,
            marginBottom: 10
        },
        categoryContainer: {
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'rgba(55, 55, 55, 0.6)',
            height: 200,
            marginBottom: 10
        },
        lblCategory: {
            color: 'white',
            fontSize: 28,
            textAlign: 'center'
        }
});
