// import libraries
import React, {Component} from 'react';
import {
    View,
    ScrollView,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import {getOrderDetail, getOrderList} from "./OrderAction";
import {Actions} from "react-native-router-flux";
import { string } from "../../localization/i18n";

class OrderHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true
        };
    }
    //
    showOrderDetail = (idOrder, token) => {
        console.log(idOrder);
        const { getOrderDetailF } = this.props;
        getOrderDetailF (idOrder, token);
    };
    // Get order data before rendering
    componentWillMount = () => {
        console.log(this.props.accessToken);
        const { getOrderListF } = this.props;
        getOrderListF(this.props.accessToken);
    };
    componentWillReceiveProps(props)
    {
        const { isGotOrderList, isGotOrderDetail } = props;
        console.log('GotOrderList: ', isGotOrderList);

        if(isGotOrderList)
        {
            this.setState ({
                isLoading:false
            })
        }
        if (isGotOrderDetail)
        {
            Actions.orderDetail();
        }
    }
    renderOrderList() {
        return this.props.orderList.map(data => {
            return (

                <View key={data.id} style={styles.orderContainer}>

                        <View style={styles.titleContainer}>
                            <TouchableOpacity key={data.id}
                                              onPress = {() => this.showOrderDetail(data.id, this.props.accessToken)}
                            >
                                <Text style={styles.txtTitle}>
                                    {string('strOrder')} #{data.id}
                                </Text>
                            </TouchableOpacity>
                        </View>

                    <View style={styles.contentContainer}>
                        {/* Merchant Info Section */}
                        <View style={styles.merchantInfoContainer}>
                            <View>
                                <Text style={styles.txtAddress}>
                                    { data.address }
                                </Text>
                            </View>
                        </View>
                        {/* Order Info Section */}
                        <View style={styles.orderInfoContainer}>
                            {/* Order Label Section */}
                            <View style={styles.lblOrderInfo}>
                                <Text style={styles.lblInfo}>ID</Text>
                                <Text style={styles.lblInfo}>{string('strCreated')}</Text>
                                <Text style={styles.lblInfo}>{string('strCompleted')}</Text>
                                <Text style={styles.lblInfo}>{string('strTotalPrice')}</Text>

                            </View>
                            {/* Order Detail Section */}
                            <View style={styles.detOrderInfo}>
                                <Text style={styles.txtInfo}>
                                    { data.id }
                                </Text>
                                <Text style={styles.txtInfo}>
                                    { data.date }
                                </Text>
                                <Text style={styles.txtInfo}>
                                    { data.date }
                                </Text>
                                <Text style={styles.txtInfo}>
                                    { data.totalPrice } đ
                                </Text>
                            </View>
                        </View>
                        {/* Status Section */}
                        <View style={styles.statusContainer}>
                            {/* Delivery Image Section */}
                            <View style={styles.deliveryImageContainer}>
                                <Text style={styles.lblInfo}>
                                    {string('strStatus')}
                                </Text>
                            </View>
                            {/* Delivery Status Section */}
                            <View style={styles.deliveryStatusContainer}>
                                <Text style={styles.txtInfo}>
                                    { data.status }
                                </Text>
                            </View>
                            {/* Order Button Section */}
                            <View style={styles.orderButtonContainer}>
                                <TouchableOpacity style={styles.btnReport}>
                                    <Text style={styles.txtButton}>
                                        {string('strCancel')}
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.btnReOrder}>
                                    <Text style={styles.txtButton}>
                                        {string('strReorder')}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            );
        })
    }

    render() {
        if (this.state.isLoading) {
            return (
                <ActivityIndicator size="large" color={"#3399ff"} />
            );
        }
        if (!this.state.isLoading) {
            return (
                <ScrollView style={styles.container}>
                    {this.renderOrderList()}
                </ScrollView>
            );
        }
    }
}
const mapStateToProps = state => ({
    accessToken: state.authenticationReducer.accessToken,
    isGotOrderList: state.orderReducer.isGotOrderList,
    orderList: state.orderReducer.orderList,
    isGotOrderDetail: state.orderReducer.isGotOrderDetail,
    orderDetail: state.orderReducer.orderDetail
});

const mapDispatchToProps = dispatch => ({
    getOrderListF: (token) => {
        dispatch(getOrderList(token));
    },
    getOrderDetailF: (idOrder, token) => {
        dispatch(getOrderDetail(idOrder,token));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderHistory);
// define styles
const styles = StyleSheet.create({
    container: {
        alignSelf: 'stretch',

    },

    // Order container style section
    orderContainer: {
        height: 200,
        marginTop: 10,

        paddingBottom: 10,
        elevation: 2,
        backgroundColor: 'white'
    },
    titleContainer: {
        flex: 2,
        backgroundColor: '#3399ff',
        alignItems: 'center',
        justifyContent: 'center'
    },
        txtTitle: {
            color: 'white',
            fontWeight: 'bold'
        },
    contentContainer: {
        flex: 8,
        paddingLeft: 20,
        paddingRight: 20,
    },
    // Merchant Info container style section
    merchantInfoContainer: {
        flex: 1,
    },
    // Order Info container style section
    orderInfoContainer: {
        flex: 6,
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtAddress: {
        fontWeight: 'bold',
        color: '#3399ff'
    },
    lblOrderInfo: {
        flex: 4
    },
        lblInfo: {
            fontStyle: 'italic'
        },
    detOrderInfo: {
        flex: 6
    },
        txtInfo: {
            fontWeight: 'bold'
        },
    // Status container style section
    statusContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    deliveryImageContainer: {
        flex: 4,
    },
    imgDelivery: {
        height: 30,
        width: 30,
    },
    deliveryStatusContainer: {
        flex: 2
    },
    orderButtonContainer: {
        flex: 4,
        flexDirection: 'row'
    },
    btnReport: {
        width: 20,
        height: 20,
        alignItems: 'center',
        backgroundColor: '#f44542',
        flex: 1,
        margin: 5,
        borderRadius: 5
    },
    btnReOrder: {
        width: 20,
        height: 20,
        alignItems: 'center',
        backgroundColor: '#56d378',
        flex: 1,
        margin: 5,
        borderRadius: 5
    },
    txtButton: {
        color: 'white',
    }
});