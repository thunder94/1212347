import {
    ORDER_REQUEST,
    ORDER_SUCCESS,
    ORDER_FAILURE,
    ORDER_DETAIL_REQUEST,
    ORDER_DETAIL_SUCCESS,
    ORDER_DETAIL_FAILURE
} from './OrderAction';

const initialState = {
    isGotOrderList: false,
    orderList: null,
    isGotOrderDetail: false,
    orderDetail: null
};

const orderReducer = (state = initialState, action) => {
    switch (action.type)
    {

        case ORDER_REQUEST:
        {
            return { ...state,
                isGotOrderList: false,
                orderList: null,
            };
        }
        case ORDER_SUCCESS:
        {
            return { ...state,
                isGotOrderList: true,
                orderList: action.message.data,
            };
        }
        case ORDER_FAILURE:
        {
            return { ...state,
                isGotOrderList: false,
                orderList: null,
            };
        }
        case ORDER_DETAIL_REQUEST:
        {
            return { ...state,
                isGotOrderDetail: false,
                orderDetail: null,
            };
        }
        case ORDER_DETAIL_SUCCESS:
        {
            return { ...state,
                isGotOrderDetail: true,
                orderDetail: action.message.data,
            };
        }
        case ORDER_DETAIL_FAILURE:
        {
            return { ...state,
                isGotOrderDetail: false,
                orderDetail: null,
            };
        }
        default:
            return state;
    }
}

export default orderReducer;