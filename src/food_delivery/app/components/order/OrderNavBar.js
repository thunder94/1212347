import {
    View,
    Text,
} from 'react-native';
import React, { Component } from 'react';
import { Actions, Router, Scene } from 'react-native-router-flux';
import { string } from "../../localization/i18n";

class OrderNavBar extends Component {
    render() {
        return (
            <View style={styles.backgroundStyle}>
                <View style={styles.navBar}>
                    <Text style={styles.lblTitle}>
                        {string('headerOrderHistory')}
                    </Text>
                </View>
            </View>
        );
    }

}
const styles = {
    backgroundStyle: {
        backgroundColor: 'white',
        flexDirection: 'column',
        justifyContent: 'center',
        height: 50
    },
    navBar: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20,
    },
    lblTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#3399ff',
        textAlign: 'center',
        justifyContent: 'center',
    },
};


export default OrderNavBar;