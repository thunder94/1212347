import axios from 'axios';

export const ORDER_REQUEST = 'ORDER_REQUEST';
export const ORDER_SUCCESS = 'ORDER_SUCCESS';
export const ORDER_FAILURE = 'ORDER_FAILURE';

export const ORDER_DETAIL_REQUEST = 'ORDER_DETAIL_REQUEST';
export const ORDER_DETAIL_SUCCESS = 'ORDER_DETAIL_SUCCESS';
export const ORDER_DETAIL_FAILURE = 'ORDER_DETAIL_FAILURE';

export const getOrderList = (token) => (dispatch) => {
    dispatch({
        type: ORDER_REQUEST,
    })

    axios.get(
        'https://food-delivery-server.herokuapp.com/order/getAll/',
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer ' + token,
            }
        }
    ).then (response => {
    console.log('Get order response: ', response);
    dispatch({
        type: ORDER_SUCCESS,
        message: response,
    })
    }).catch(e => {
        console.log('Get order list failed: ', e);
        dispatch({
            type: ORDER_FAILURE,
            message: e,
        })
    })
}

export const getOrderDetail = (idOrder, token) => (dispatch) => {
    dispatch({
        type: ORDER_DETAIL_REQUEST,
    })

    axios.get(
        'https://food-delivery-server.herokuapp.com/order/getOrder/' + idOrder,
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer ' + token,
            }
        }
    ).then (response => {
        console.log('Get order detail response: ', response);
        dispatch({
            type: ORDER_DETAIL_SUCCESS,
            message: response,
        })
    }).catch(e => {
        console.log('Get order detail failed: ', e);
        dispatch({
            type: ORDER_DETAIL_FAILURE,
            message: e,
        })
    })
}
