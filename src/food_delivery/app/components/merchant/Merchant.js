// import libraries
import React, {Component} from 'react';
import {
    View,
    ScrollView,
    Text,
    TextInput,
    Image,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
} from 'react-native';
import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import { getMerchantDetail } from "./MerchantAction";
import { connect } from 'react-redux';
import { string } from "../../localization/i18n";

class Merchant extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            content: [],
            isLoading: true,
            strSearch: ''
        };
    };
    // Handle text input
    handleSearchString = (text) => {
        this.setState({ strSearch: text })
    };
    // Search merchant by name
    searchMerchant = () => {
        this.setState({
            isLoading: true
        });
        console.log(this.state.strSearch);
        axios.get(
            'https://food-delivery-server.herokuapp.com/restaurant/search?name=' + this.state.strSearch,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            }
        ).then (response => {
            console.log('Response: ', response);
            var data = response.data;
            console.log(data);
            if (data.length > 0) {
                this.setState({
                    content: data,
                    isLoading: false
                });
            }
        }).catch(e => {
            console.log('Get failed: ', e);

        });
    };
    // Show merchant detail
    showMerchantDetail = (idMerchant) => {
        console.log(idMerchant);
        const { getMerchantDetailF } = this.props;
        getMerchantDetailF (idMerchant);
    };
    componentWillReceiveProps(props)
    {
        const { isGotDetail } = props;
        console.log('isGotDetail: ', isGotDetail);

        if(isGotDetail)
        {
            Actions.merchantDetail();
        }

    }
    // Render functions
    renderRating(rating) {
        var result = [];
        for (var i = 0; i < rating; i++) {
            result.push (
                <Image key = {i}
                       style={styles.imgRating}
                       source={require('../images/star.png')}
                />
            )
        }
        return result;
    };
    renderMerchantList() {
        return this.state.content.map(content => {
            return (
                <TouchableOpacity key={content.id}
                                  style={styles.itemContainer}
                                  onPress = {() => this.showMerchantDetail(content.id)} >
                    <Image style={styles.img} source={{uri: content.image}} />
                    <View style={styles.merchantInfo}>
                        <Text style={styles.lblMerchantName}>
                            {content.name}
                        </Text>
                        <View style={styles.rating}>
                            {this.renderRating(content.rating)}
                        </View>
                        <Text style={styles.lblTime}>
                            {content.timeOpen} - {content.timeClose}
                        </Text>
                    </View>
                </TouchableOpacity>
            );
        })
    };
    componentWillMount = () => {

    };
    render() {
        if (this.state.isLoading) {
            return (
                <View style={styles.container}>
                    <View style={styles.searchContainer}>
                        <TextInput style={styles.txtInput}
                                   placeholder={string('strMerchantSearchText')}
                                   underlineColorAndroid={'transparent'}
                                   returnKeyType={'search'}
                                   blurOnSubmit={true}
                                   onSubmitEditing={this.searchMerchant}
                                   onChangeText = {this.handleSearchString}
                        />
                    </View>
                </View>

            );
        }
        if (!this.state.isLoading) {
            return (
                <View style={styles.container}>
                    <View style={styles.searchContainer}>
                        <TextInput style={styles.txtInput}
                                   placeholder={string('strMerchantSearchText')}
                                   underlineColorAndroid={'transparent'}
                                   autoCapitalize={'none'}
                                   returnKeyType={'search'}
                                   blurOnSubmit={true}
                                   onSubmitEditing={this.searchMerchant}
                                   onChangeText = {this.handleSearchString}
                        />
                    </View>
                    <View style={styles.merchantContainer}>
                        <ScrollView>
                            {this.renderMerchantList()}
                        </ScrollView>
                    </View>
                </View>
            );
        }

    }
}
const mapStateToProps = state => ({
    isGotDetail: state.merchantReducer.isGotDetail,
});

const mapDispatchToProps = dispatch => ({
    getMerchantDetailF: (idMerchant) => {
        dispatch(getMerchantDetail(idMerchant));
    }
});
export default connect(mapStateToProps, mapDispatchToProps)(Merchant);

// define styles
const styles = StyleSheet.create({

    container: {
        alignSelf: 'stretch',
        paddingLeft: 10,
        paddingRight: 10,
        flex: 1
    },

    searchContainer: {
        justifyContent: 'center',
        backgroundColor: '#fff',
        height: 50,
        marginTop: 20,
        borderRadius: 5
    },
        inputContainer: {
        },
            txtInput: {
                backgroundColor: '#fff',
                paddingLeft: 10
            },
    iconLoadingContainer: {
        height: 300,
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center'
    },
    merchantContainer: {
        marginTop: 20,
    },
        itemContainer: {
            flexDirection: 'row',
            height: 60,
            borderRadius: 5,
            backgroundColor: 'white',
            elevation: 3,
            marginBottom: 10
        },
            img: {
                flex: 2,
                flexDirection: 'row',
                width: 40,
                height: 40,
                margin: 10
            },
            merchantInfo: {
                flex: 8,
                flexDirection: 'column'
            },
            lblMerchantName: {
                flex: 3,
                fontSize: 10
            },
            rating: {
                flex: 4,
                flexDirection: 'row'
            },
                imgRating: {
                    height: 10,
                    width: 10,
                },
            lblTime: {
                flex: 3
            }
});