import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Image,
} from 'react-native';

import MapView, { Callout, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { getMerchantLocation } from "./MerchantAction";
import { connect } from 'react-redux';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 10.773533;
const LONGITUDE = 106.702899;
const LATITUDE_DELTA = 0.01; // If delta is smaller, the map is more detailed
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

function randomColor() {
    return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
}

class Map extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },
        };
    }
    getMerchantLocation = (lat, long) => {
        const { getMerchantLocationF } = this.props;
        getMerchantLocationF(lat, long);
    };
    onMapPress(e) {
        console.log(this.props.merchantLocationList);
        console.log('onMapPress: ', e);
    }
    componentDidMount = () => {
        this.getMerchantLocation(this.state.region.latitude, this.state.region.longitude);
    };
    render() {
        return (
            <View style={styles.container}>

                <MapView
                    provider={PROVIDER_GOOGLE}
                    style={styles.map}
                    initialRegion={this.state.region}
                    onPress={e => this.onMapPress(e)}
                    onRegionChangeComplete={
                        (region) => {
                            var lat = parseFloat(region.latitude);
                            var long = parseFloat(region.longitude);
                            console.log(lat, long);
                            this.getMerchantLocation(lat, long);
                        }
                    }
                >

                    {this.props.merchantLocationList.map(data => (
                        <Marker
                            key = {data.id}
                            coordinate={
                                {
                                    latitude: data.latitude,
                                    longitude: data.longitude
                                }
                            }
                        >
                            <Image style={styles.img} source={require('../images/restaurant.png')}/>
                            <Callout style={styles.callout}>
                                <Text style={styles.txtCallout}>
                                    {data.RESTAURANT.name}
                                </Text>
                                <Text>
                                    Rating: {data.RESTAURANT.rating} | {data.street}
                                </Text>
                            </Callout>
                        </Marker>
                    ))}

                </MapView>


            </View>
        );
    }
}

const mapStateToProps = state => ({
    merchantLocationList: state.merchantReducer.merchantLocationList,
});

const mapDispatchToProps = dispatch => ({
    getMerchantLocationF: (lat, long) => {
        dispatch(getMerchantLocation(lat, long));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Map);
const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    img: {
        height: 20,
        width: 20,
    },
    callout: {
        height: 40,
        width: 300
    },
        txtCallout: {
            fontWeight: 'bold',
            fontSize: 12,
            textAlign: 'center'
        },

});

