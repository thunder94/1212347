import {
    View,
    Image,
    Text,
    StatusBar,
    TouchableWithoutFeedback
} from 'react-native';
import React, { Component } from 'react';
import { Actions, Router, Scene } from 'react-native-router-flux';
import {string} from "../../localization/i18n";

class MapNavBar extends Component {
    render() {
        return (
            <View style={styles.backgroundStyle}>
                <View style={styles.navBar}>
                    <Text style={styles.lblTitle}>
                        {string('headerMerchantNearby')}
                    </Text>
                    <TouchableWithoutFeedback onPress={() => Actions.merchant()}>
                        <Image
                            source={require('../images/list.png')}
                            style={styles.locationStyle} />
                    </TouchableWithoutFeedback>

                </View>
            </View>
        );
    }

}
const styles = {
    backgroundStyle: {
        backgroundColor: 'white',
        flexDirection: 'column',
        justifyContent: 'center',
        height: 50
    },
    navBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 20,
        paddingRight: 20,
    },
    lblTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#3399ff',
        textAlign: 'center',
        justifyContent: 'center',
        paddingTop: 10
    },
    locationStyle: {
        resizeMode: 'contain',
        width: 30,
        height: 40,
        justifyContent: 'flex-end',
        position: 'relative',
    }
};


export default MapNavBar;