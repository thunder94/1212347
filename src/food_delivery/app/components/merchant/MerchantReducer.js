import {
    MERCHANT_DETAIL_REQUEST,
    MERCHANT_DETAIL_SUCCESS,
    MERCHANT_DETAIL_FAILURE,
    MERCHANT_LOCATION_REQUEST,
    MERCHANT_LOCATION_SUCCESS,
    MERCHANT_LOCATION_FAILURE,
    ADD_TO_CART,
    INCREASE_ITEM_NUMBER,
    DECREASE_ITEM_NUMBER
} from './MerchantAction';

const initialState = {
    isGotDetail: false,
    merchantDetail: null,
    merchantMenu: null,
    merchantLocationList: [],
    cartList: '',
};
function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
}

const merchantReducer = (state = initialState, action) => {
    switch (action.type)
    {

        case MERCHANT_DETAIL_REQUEST:
        {
            return { ...state,
                isGotDetail: false,
                merchantDetail: null,
            };
        }
        case MERCHANT_DETAIL_SUCCESS:
        {
            return { ...state,
                isGotDetail: true,
                merchantDetail: action.message.data.restaurant,
                merchantMenu: action.message.data.menu,
            };
        }
        case MERCHANT_DETAIL_FAILURE:
        {
            return { ...state,
                isGotDetail: false,
                merchantDetail: null,
                merchantMenu: null
            };
        }
        case MERCHANT_LOCATION_REQUEST:
        {
            return { ...state,

            };
        }
        case MERCHANT_LOCATION_SUCCESS:
        {
            return { ...state,
                merchantLocationList: action.message.data,
            }
        }
        case MERCHANT_LOCATION_FAILURE:
        {
            return { ...state,

            }
        }
        case ADD_TO_CART:
        {
            const newItem = action.message;
            return { ...state,
                cartList: [
                    ...state.cartList,
                    newItem
                ]
            }
        }
        case INCREASE_ITEM_NUMBER:
        {
            const idItem = action.id;
            var key = 0;
            var newCart = {};
            console.log(state.cartList);
            state.cartList.map(data => {
                if(idItem === data.id) {
                    key = getKeyByValue(state.cartList, data);
                    console.log(state.cartList[key]);
                    newCart =  { ...state,
                        cartList: {
                            ...state.cartList,
                            [key]: {
                                ...state.cartList[key],
                                number: state.cartList[key].number + 1
                            }
                        }
                    };
                }
            });

            return { ...state,
                cartList: {
                    ...state.cartList,
                    [key]: {
                        ...state.cartList[key],
                        number: state.cartList[key].number + 1
                    }
                }
            }

        }
        default:
            return state;
    }
}

export default merchantReducer;