import axios from 'axios';

export const MERCHANT_DETAIL_REQUEST = 'MERCHANT_DETAIL_REQUEST';
export const MERCHANT_DETAIL_SUCCESS = 'MERCHANT_DETAIL_SUCCESS';
export const MERCHANT_DETAIL_FAILURE = 'MERCHANT_DETAIL_FAILURE';

export const MERCHANT_LOCATION_REQUEST = 'MERCHANT_LOCATION_REQUEST';
export const MERCHANT_LOCATION_SUCCESS = 'MERCHANT_LOCATION_SUCCESS';
export const MERCHANT_LOCATION_FAILURE = 'MERCHANT_LOCATION_FAILURE';

export const ADD_TO_CART = 'ADD_TO_CART';
export const INCREASE_ITEM_NUMBER = 'INCREASE_ITEM_NUMBER';
export const DECREASE_ITEM_NUMBER = 'DECREASE_ITEM_NUMBER';

export const PURCHASE = 'PURCHASE';

export const getMerchantDetail = (idMerchant) => (dispatch) => {
    dispatch({
        type: MERCHANT_DETAIL_REQUEST,
    });

    axios.get('https://food-delivery-server.herokuapp.com/restaurant/getMenu/' + idMerchant).then (response => {
        console.log('Merchant detail response: ', response);
        dispatch({
            type: MERCHANT_DETAIL_SUCCESS,
            message: response,
        })
    }).catch(e => {
        console.log('Get merchant detail failed: ', e);
        dispatch({
            type: MERCHANT_DETAIL_FAILURE,
            message: e,
        })
    })
};
export const getMerchantLocation = (lat, long) => (dispatch) => {
    dispatch({
        type: MERCHANT_LOCATION_REQUEST,
    });

    axios.get('https://food-delivery-server.herokuapp.com/restaurant/nearMe/' + lat + '&'+ long).then (response => {
        console.log('Merchant location response: ', response);
        dispatch({
            type: MERCHANT_LOCATION_SUCCESS,
            message: response,
        })
    }).catch(e => {
        console.log('Get merchant location failed: ', e);
        dispatch({
            type: MERCHANT_LOCATION_FAILURE,
            message: e,
        })
    })
};
export const addToCart = (product) => (dispatch) => {
    dispatch({
        type: ADD_TO_CART,
        message: product
    });
};

export const increaseItemNumber = (idItem) => (dispatch) => {
    dispatch({
        type: INCREASE_ITEM_NUMBER,
        id: idItem
    });
};

export const purchase  = (data) => (dispatch) => {
    dispatch({
        type: PURCHASE,
        data: data
    });
}