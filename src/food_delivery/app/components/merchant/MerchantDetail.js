// import libraries
import React, {Component} from 'react';
import {
    View,
    ScrollView,
    Text,
    TextInput,
    Image,
    ImageBackground,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import { getMerchantDetail, addToCart } from "./MerchantAction";
import { connect } from 'react-redux';

class MerchantDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    };
    componentWillMount = () => {
        console.log(this.props.merchantDetail);
        console.log(this.props.merchantMenu);
    };
    addToCart = (data) => {
        // Add number property to product
        var product = data;
        product.number = 1;
        console.log(product);
        const { addToCartF } = this.props;
        addToCartF(product);
    };
    renderRating(rating) {
        var result = [];
        for (var i = 0; i < rating; i++) {
            result.push (
                <Image key = {i}
                       style={styles.imgRating}
                       source={require('../images/star.png')}
                />
            )
        }
        return result;
    };
    renderMenu = () => {
        return this.props.merchantMenu.map(data => {
           return (
               <View key={data.id}
                     style={styles.itemContainer}>
                   <Image style={styles.imgItem} source={{uri: data.image}}/>

                   <View style={styles.foodInfoContainer}>
                       <View style={styles.foodNameContainer}>
                           <Text style={styles.txtFoodName}>
                               {data.name}
                           </Text>
                       </View>

                       <View style={styles.foodCostContainer}>
                           <Text style={styles.txtFoodCost}>
                               {data.price} VNĐ
                           </Text>
                       </View>

                       <View style={styles.soldNumberContainer}>
                           <Text style={styles.txtSoldNumber}>
                               Đã bán: {data.sold}
                           </Text>
                       </View>
                   </View>

                   <View style={styles.buttonContainer}>
                       <TouchableOpacity
                           style={styles.btnAddItem}
                           onPress={() => this.addToCart(data)}
                       >
                           <Text style={styles.txtAddItem}>
                               Pick
                           </Text>
                       </TouchableOpacity>
                   </View>
               </View>
           ) ;
        });
    };
    render() {
        const { merchantDetail } = this.props;
        return (
            <View style={styles.container}>
                <ScrollView>
                    {/* Merchant info section */}
                    <View style={styles.merchantInfoContainer}>
                        <ImageBackground style={styles.merchantImage}
                                         source={{uri: merchantDetail.image}}>
                            <View style={styles.merchantInfo}>
                                <Text style={styles.txtMerchantStatus}>
                                    Đang mở cửa
                                </Text>
                                <Text style={styles.txtMerchantName}>
                                    {merchantDetail.name}
                                </Text>
                                <Text style={styles.txtMerchantAddress}>
                                    123 đường 3/2, quận Bình Thạnh, TP.HCM
                                </Text>
                            </View>
                        </ImageBackground>
                    </View>
                    {/* Info section */}
                    <View style={styles.infoContainer}>

                        <View style={styles.verifyContainer}>
                            <Image style={styles.imgVerify} source={require('../images/verify.png')}/>
                        </View>

                        <View style={styles.deliveryCostContainer}>
                            <View style={styles.lblDeliveryCost}>
                                <Text style={styles.infoLabel}>Phí giao hàng</Text>
                            </View>
                            <View style={styles.txtDeliveryCost}>
                                <Text style={styles.infoText}>8,500đ</Text>
                            </View>
                        </View>

                        <View style={styles.ratingContainer}>
                            <View style={styles.lblRating}>
                                <Text style={styles.infoLabel}>Đánh giá</Text>
                            </View>
                            <View style={styles.txtRating}>
                                <Text style={styles.infoText}>
                                    {this.renderRating(merchantDetail.rating)}
                                </Text>
                            </View>
                        </View>

                        <View style={styles.commentContainer}>
                            <View style={styles.lblComment}>
                                <Text style={styles.infoLabel}>Bình luận</Text>
                            </View>
                            <View style={styles.txtComment}>
                                <Text style={styles.infoText}>22 bình luận</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.menuContainer}>
                        {this.renderMenu()}
                    </View>
                </ScrollView>


            </View>
        );
    }
}
const mapStateToProps = state => ({
    isGotDetail: state.merchantReducer.isGotDetail,
    merchantDetail: state.merchantReducer.merchantDetail,
    merchantMenu: state.merchantReducer.merchantMenu,
    cartList: state.merchantReducer.cartList,
});

const mapDispatchToProps = dispatch => ({
    getMerchantDetailF: (idMerchant) => {
        dispatch(getMerchantDetail(idMerchant));
    },
    addToCartF: (product) => {
        dispatch(addToCart(product));
    }
});
export default connect(mapStateToProps, mapDispatchToProps)(MerchantDetail);
// define styles
const styles = StyleSheet.create({
    container: {
        alignSelf: 'stretch'

    },
    // Merchant info style section
    merchantInfoContainer: {

    },
    merchantInfo: {
      flex: 1,
      justifyContent: 'flex-end',
      backgroundColor: 'rgba(52, 52, 52, 0.4)'
    },
    txtMerchantStatus: {
        color: 'white',
        fontSize: 10,
    },
    txtMerchantName: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold'
    },
    txtMerchantAddress: {
        color: 'white',
        fontSize: 12,
    },
    merchantImage: {
        width: '100%',
        height: 200
    },
    // Info style section
    infoContainer: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        borderTopColor: 'black',
        borderTopWidth: 1,
        borderBottomColor: 'black',
        borderBottomWidth: 1,

    },
    verifyContainer: {
        flex: 1,
        alignItems: 'center',
    },
        imgVerify: {
            height: 40,
            width: 40,
        },
    deliveryCostContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column'
    },
        lblDeliveryCost: {
            flex: 1
        },
        txtDeliveryCost: {
            flex: 1
        },
    ratingContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column'
    },
        lblRating: {
            flex: 1
        },
        txtRating: {
            flex: 1
        },
            imgRating: {
                height: 15,
                width: 15
            },
    commentContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column'
    },
        lblComment: {
            flex: 1
        },
        txtComment: {
            flex: 1
        },
    infoLabel: {
        fontWeight: 'bold',
        color: '#3399ff'
    },
    infoText: {

    },
    // Item style section
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        borderBottomColor: '#000000',
        borderBottomWidth: 0.5,
        justifyContent: 'center'
    },
    imgItem: {
        flex: 2,
        flexDirection: 'row',
        width: 50,
        height: 50,
        margin: 10
    },
    // Food info style section
    foodInfoContainer: {
        flex: 8,

    },
    foodNameContainer: {
        flex: 2
    },
    foodCostContainer: {
        flex: 1
    },
    soldNumberContainer: {
        flex: 1

    },
    txtFoodName: {
        fontSize: 12,
        fontWeight: 'bold'
    },
    txtFoodCost: {
        fontSize: 12,
    },
    txtSoldNumber: {
        fontSize: 10,
        justifyContent: 'flex-end'
    },
    // Button style section
    buttonContainer: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnAddItem: {
        backgroundColor: '#3399ff',
        justifyContent: 'center',
        height: 30,
        width: 40,
        borderRadius: 5
    },
    txtAddItem: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center'
    }
});

