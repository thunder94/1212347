// import libraries
import React, {Component} from 'react';
import {
    View,
    ScrollView,
    Text,
    TextInput,
    Image,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import { increaseItemNumber, purchase } from "./MerchantAction";
import ButtonStandard from "../ButtonStandard";
import { string } from "../../localization/i18n";

class Basket extends Component {
    constructor(props) {
        super(props);

    };
    decreaseItemNumber = (idItem) => {
        const { decreaseItemNumberF } = this.props;
        decreaseItemNumberF(idItem);
        console.log(this.props.cartList);
    };
    increaseItemNumber = (idItem) => {
        const { increaseItemNumberF } = this.props;
        increaseItemNumberF(idItem);
    };
    purchase = () => {
        const { purchaseF } = this.props;
        // Calculate totalPrice and generate itemList
        var itemList = [];
        var totalPrice = 0;
        for (element of this.props.cartList)
        {
            totalPrice = totalPrice + element.price * element.number;
            let item = {
                'idFood': element.id,
                'quantity': element.number,
                'note': ''
            };
            itemList.push(item);
        }
        console.log(itemList);
        // Create data
        var data = {
            'totalPrice': totalPrice,
            'Street': this.props.userInfo.street,
            'idWard': this.props.userInfo.idWard,
            'idDistrict': this.props.userInfo.idDistrict,
            'phone': this.props.userInfo.phone,
            'item': itemList
        }
        console.log(data);
        //purchaseF();
    };
    renderItem = () => {
        return this.props.cartList.map(data => {
            return (
                <View key={data.id}
                      style={styles.itemContainer}>

                    <Image style={styles.imgItem} source={{uri: data.image}}/>

                    <View style={styles.foodInfoContainer}>
                        <View style={styles.foodNameContainer}>
                            <Text style={styles.txtFoodName}>
                                {data.name}
                            </Text>
                        </View>

                        <View style={styles.foodCostContainer}>
                            <Text style={styles.txtFoodCost}>
                                {data.number} * {data.price} = {data.number * data.price} VNĐ
                            </Text>
                        </View>
                    </View>

                    <View style={styles.numberContainer}>
                        <TouchableOpacity style={styles.btnReduce} onPress={() => this.decreaseItemNumber(data.id)}>
                            <Text style={styles.lblButtonNumber}>
                                -
                            </Text>
                        </TouchableOpacity>
                        <View style={styles.viewTextNumber}>
                            <Text style={styles.txtNumber}>
                                {data.number}
                            </Text>
                        </View>
                        <TouchableOpacity style={styles.btnAdd} onPress={() => this.increaseItemNumber(data.id)}>
                            <Text style={styles.lblButtonNumber}>
                                +
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            ) ;
        });
    };
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.viewLabelImage}>
                        <Text style={styles.label}>
                            {string('strImage')}
                        </Text>
                    </View>
                    <View style={styles.viewLabelName}>
                        <Text style={styles.label}>
                            {string('strFoodName')}
                        </Text>
                    </View>
                    <View style={styles.viewLabelNumber}>
                        <Text style={styles.label}>
                            {string('strNumber')}
                        </Text>
                    </View>
                </View>
                <ScrollView>
                    {this.renderItem()}
                </ScrollView>
                <View>
                    <ButtonStandard text={string('strPurchase')} onPress={() => this.purchase()}/>
                </View>
            </View>

        );
    }
}
const mapStateToProps = state => ({
    cartList: state.merchantReducer.cartList,
    userInfo: state.authenticationReducer.userInfo,
});

const mapDispatchToProps = dispatch => ({
    decreaseItemNumbernF: (idItem) => {
        dispatch(decreaseItemNumber(idItem));
    },
    increaseItemNumberF: (idItem) => {
        dispatch(increaseItemNumber(idItem));
    },
    purchaseF: (data) => {
        dispatch(purchase(data));
    },
});
export default connect(mapStateToProps, mapDispatchToProps)(Basket);
// define styles

const styles = StyleSheet.create({
    container: {

    },
    header: {
        height: 20,
        flexDirection: 'row',
    },
        viewLabelImage: {
            flex: 2
        },
        viewLabelName: {
            flex: 5
        },
        viewLabelNumber: {
            flex: 3
        },
        label: {
            fontWeight: 'bold',
            fontSize: 14,
            textAlign: 'center'
        },
    // Item style section
    itemContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        borderTopColor: '#000000',
        borderTopWidth: 0.5,
        justifyContent: 'center',
    },
        imgItem: {
            flex: 2,
            flexDirection: 'row',
            width: 50,
            height: 50,
            margin: 10
        },
        // Food info style section
        foodInfoContainer: {
            flex: 5,

        },
            foodNameContainer: {
                flex: 2
            },
            foodCostContainer: {
                flex: 1
            },
                txtFoodName: {
                    fontSize: 12,
                    fontWeight: 'bold'
                },
                txtFoodCost: {
                    fontSize: 12,
                },
        numberContainer: {
            flex: 3,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center'
        },
            btnReduce: {
                flex: 1,
                height: 20,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#f44542'
            },
            btnAdd: {
                flex: 1,
                height: 20,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#56d378',
            },
            lblButtonNumber: {
                textAlign: 'center',
                color: 'white',
                fontWeight: 'bold'
            },
            viewTextNumber: {
                flex: 1,
            },
                txtNumber: {
                    textAlign: 'center'
                }
});
