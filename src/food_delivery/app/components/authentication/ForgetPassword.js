// import libraries
import React, {Component} from 'react';
import {
    Alert,
    View,
    Text,
    TextInput,
    StyleSheet,
    TouchableOpacity, Image,
} from 'react-native';
import ButtonStandard from '../ButtonStandard';
import { string, changeLanguage } from "../../localization/i18n";

// Alert Functions
const alertSuccess = (msg) => {
    Alert.alert(msg);
};

export default class ForgetPassword extends Component {

    state: {
        email: ''
    };
    // Handle TextInput functions
    handleEmail = (text) => {
        this.setState({ email: text })
    };
    sendForgetRequest = (email) => {
        fetch('https://food-delivery-server.herokuapp.com/forgetPassword', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
            }),
        })
        // Handle responses
            .then(function(response){
                if(response.status == 200){
                    alertSuccess('An email has been sent to ' + email);
                };
                if(response.status == 400){
                    console.log ('Email is wrong');
                };
                if(response.status == 500){
                    console.log ('');
                };
                console.log(response);
                return response.json();
            });
    };

    render() {
        return (
            <View style={styles.container}>

                <Text style={styles.header}>{string('strForget')}</Text>

                <Text style={styles.txtBody}>{string('strForgetContent')}</Text>
                <TextInput style={styles.txtInput}
                           placeholder={string('strEmail')}
                           underlineColorAndroid={'transparent'}
                           autoCapitalize = "none"
                           onChangeText = {this.handleEmail}/>


                <ButtonStandard text={string('strResetPassword')} onPress={() => this.sendForgetRequest(this.state.email)}/>

            </View>
        );
    }
}

// define styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: 60,
        paddingLeft: 60,
    },
    img: {
        width: 160,
        height: 160,
        marginTop: 20
    },
    header: {
        fontSize: 24,
        color: '#3399ff',
        paddingBottom: 10,
        marginBottom: 40,
    },
    txtBody: {
        fontSize: 12,

        marginBottom: 40,
        textAlign: 'center'
    },
    txtInput: {
        alignSelf: 'stretch',
        height: 40,
        marginTop: 20,
        color: '#000000',
        backgroundColor: 'white',
        borderColor: 'lightgrey',
        borderWidth: 1,
        elevation: 2

    },
});