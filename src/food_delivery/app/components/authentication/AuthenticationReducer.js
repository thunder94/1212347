import {
    AUTHENTICATION_SUCCESS,
    AUTHENTICATION_FAILURE,
    AUTHENTICATION_REQUEST,
    LOG_OUT,
    GET_INFO_REQUEST,
    GET_INFO_SUCCESS,
    GET_INFO_FAILURE,
    UPDATE_INFO_REQUEST,
    UPDATE_INFO_SUCCESS,
    UPDATE_INFO_FAILURE
} from './AuthenticationAction';

const initialState = {
    isAuthenticating: false,
    isAuthenticated: false,
    isGotInfo: false,
    userInfo: null,
    accessToken: null,
}

const authenticationReducer = (state = initialState, action) => {
    switch (action.type)
    {
        case AUTHENTICATION_SUCCESS:
        {
            return { ...state,
                isAuthenticating: false,
                isAuthenticated: true,
                accessToken: action.message.data.token,
            };
        }
        case AUTHENTICATION_REQUEST:
        {
            return { ...state,
                isAuthenticating: true,
                isAuthenticated: false,
                accessToken: null,
            };
        }
        case AUTHENTICATION_FAILURE:
        {
            return { ...state,
                isAuthenticated: false,
                isAuthenticating: false,
                accessToken: null,
            };
        }
        case LOG_OUT:
        {
            return { ...state,
                isAuthenticated: false,
                accessToken: null,
            };
        }

        case GET_INFO_REQUEST:
        {
            return { ...state,

            };
        }
        case GET_INFO_SUCCESS:
        {
            return { ...state,
                userInfo: action.message.data,
                isGotInfo: true,
            };
        }
        case GET_INFO_FAILURE:
        {
            return { ...state,
                userInfo: null,
                isGotInfo: false,
            };
        }
        case UPDATE_INFO_REQUEST:
        {
            return { ...state,
            };
        }
        case UPDATE_INFO_SUCCESS:
        {
            return { ...state,
                userInfo: action.info,
            };
        }
        case UPDATE_INFO_FAILURE:
        {
            return { ...state,
            };
        }
        default:
            return state;
    }
}

export default authenticationReducer;