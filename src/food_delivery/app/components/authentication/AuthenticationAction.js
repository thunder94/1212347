import axios from 'axios';

export const AUTHENTICATION_REQUEST = 'AUTHENTICATION_REQUEST';
export const AUTHENTICATION_SUCCESS = 'AUTHENTICATION_SUCCESS';
export const AUTHENTICATION_FAILURE = 'AUTHENTICATION_FAILURE';
export const LOG_OUT = 'LOG_OUT';

export const GET_INFO_REQUEST = 'GET_INFO_REQUEST';
export const GET_INFO_SUCCESS = 'GET_INFO_SUCCESS';
export const GET_INFO_FAILURE = 'GET_INFO_FAILURE';

export const UPDATE_INFO_REQUEST = 'UPDATE_INFO_REQUEST';
export const UPDATE_INFO_SUCCESS = 'UPDATE_INFO_SUCCESS';
export const UPDATE_INFO_FAILURE = 'UPDATE_INFO_FAILURE';

export const authenticate = (data) => (dispatch) => {
    dispatch({
        type: AUTHENTICATION_REQUEST,
    })

    axios.post('https://food-delivery-server.herokuapp.com/login', data).then (response => {
        console.log('Login response: ', response);
        dispatch({
            type: AUTHENTICATION_SUCCESS,
            message: response,
        });
        dispatch({
            type: GET_INFO_REQUEST,
        });
        axios.get(
            'https://food-delivery-server.herokuapp.com/getinfo',
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'bearer ' + response.data.token,
                }
            }
        ).then (responseGet => {
            console.log('Response: ', responseGet);
            dispatch({
                type: GET_INFO_SUCCESS,
                message: responseGet,
            })
        }).catch(e => {
            console.log('Get failed: ', e);
            dispatch({
                type: GET_INFO_FAILURE,
                message: e,
            })
        })
    }).catch(e => {
        console.log('Login failed: ', e);
        dispatch({
            type: AUTHENTICATION_FAILURE,
            message: e,
        })
    })
}

export const logOut = () => (dispatch) => {
    dispatch({
        type: LOG_OUT,
    });
}

export const getUserInfo = (token) => (dispatch) => {
    dispatch({
        type: GET_INFO_REQUEST,
    });
    axios.get(
            'https://food-delivery-server.herokuapp.com/getinfo',
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'bearer ' + token,
                }
            }
        ).then (response => {
        console.log('Response: ', response);
        dispatch({
            type: GET_INFO_SUCCESS,
            message: response,
        })
    }).catch(e => {
        console.log('Get failed: ', e);
        dispatch({
            type: GET_INFO_FAILURE,
            message: e,
        })
    })
}

export const updateUserInfo = (token, data) => (dispatch) => {
    dispatch({
        type: UPDATE_INFO_REQUEST,
    });
    console.log(data);
    console.log(JSON.parse(data));
    axios.post(
            'https://food-delivery-server.herokuapp.com/updateInfo',
            data,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'bearer ' + token,
                }
            }
        ).then (response => {
        console.log('Response: ', response);
        dispatch({
            type: UPDATE_INFO_SUCCESS,
            info: JSON.parse(data),
        })
    }).catch(e => {
        console.log('Update failed: ', e);
        dispatch({
            type: UPDATE_INFO_FAILURE,
            message: e,
        })
    })
}