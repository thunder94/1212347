// import libraries
import React, {Component} from 'react';
import {
    Alert,
    View,
    Text,
    TextInput,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import ButtonStandard from '../ButtonStandard';
import {string} from "../../localization/i18n";


// Alert Functions
const alertRegisterSuccess = () => {
    Alert.alert('Congratulations! You have been registered successfully!');
};
const alertExistedEmail = () => {
    Alert.alert('This email is already existed!');
};
const alertInvalidEmail = () => {
    Alert.alert('This email is invalid!');
};

export default class Register extends Component {
    state: {
        email: '',
        password: ''
    };
    // Handle TextInput functions
    handleEmail = (text) => {
        this.setState({ email: text })
    };
    handlePassword = (text) => {
        this.setState({ password: text })
    };
    // Send JSON register data to server
    register = (email, password) => {
        fetch('https://food-delivery-server.herokuapp.com/register', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password
            }),
        })
        // Handle responses
        .then(function(response){
            if(response.status == 200){
                alertRegisterSuccess();
                console.log ('Success!');
            };
            if(response.status == 400){
                alertExistedEmail();
                console.log ('Email already exists');
            };
            if(response.status == 401){
                alertInvalidEmail();
                console.log ('Email is invalid');
            };
            if(response.status == 500){
                console.log ('Other error');
            };
            console.log(response);
            return response.json();

        });
    };
    render() {
        return (
            <View style={styles.container}>

                <Text style={styles.header}>{string('strCreateAccount')}</Text>


                <TextInput style={styles.txtInput}
                           placeholder={string('strEmail')}
                           underlineColorAndroid={'transparent'}
                           autoCapitalize = "none"
                           onChangeText = {this.handleEmail}/>
                <TextInput style={styles.txtInput}
                           placeholder={string('strPassword')}
                           secureTextEntry={true}
                           underlineColorAndroid={'transparent'}
                           autoCapitalize = "none"
                           onChangeText = {this.handlePassword}/>

                <ButtonStandard text={string('strSignUp')} onPress={() => this.register(this.state.email, this.state.password)} />

            </View>
        );
    }
}

// define styles
const styles = StyleSheet.create({
   container: {
       flex: 1,
       justifyContent: 'center',
       alignItems: 'center',
       paddingRight: 60,
       paddingLeft: 60,
   },
   header: {
       fontSize: 24,
       color: '#3399ff',
       paddingBottom: 10,
   },
    txtInput: {
        alignSelf: 'stretch',
        height: 40,
        marginTop: 20,
        color: '#000000',
        backgroundColor: 'white',
        borderColor: 'lightgrey',
        borderWidth: 1,
        elevation: 2
    }
});