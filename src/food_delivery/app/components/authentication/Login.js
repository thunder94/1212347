// import libraries
import React, {Component} from 'react';
import {
    Alert,
    View,
    Text,
    TextInput,
    Image,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import ButtonStandard from '../ButtonStandard';
import * as Animatable from 'react-native-animatable';
import { Actions } from 'react-native-router-flux';
import { authenticate } from "./AuthenticationAction";
import { connect } from 'react-redux';
import { string, changeLanguage } from "../../localization/i18n";

// Alert Functions
const alertInvalidInput = () => {
    Alert.alert('Your email and password does not match!');
};
const alertAccountNotVerified = () => {
    Alert.alert('This email is not verified!');
};

type Props = {};

class Login extends Component<Props> {

    constructor(props) {
        super(props);
    }
    state: {
        email: '',
        password: '',
    };

    // Handle TextInput
    handleEmail = (text) => {
        this.setState({ email: text })
    };
    handlePassword = (text) => {
        this.setState({ password: text })
    };
    // Login
    login = (email, password) => {
        const { authenticateF } = this.props;
        console.log(email + password);
        authenticateF({
            email: email,
            password: password
        })
    };
    componentDidMount()
    {
        const { isAuthenticated } = this.props;
        console.log('componentDidMount, isAuthenticated: ', isAuthenticated);
        if (isAuthenticated) {
            setTimeout(() => {
                Actions.merchantRoot();
            }, 100);
        }

    }

    componentWillReceiveProps(props)
    {
        const { isAuthenticated } = props;
        console.log('isAuthenticated: ', isAuthenticated);

        if(isAuthenticated)
        {
            Actions.merchantRoot();
        }

    }

    render() {
        return (
            <Animatable.View style={styles.container} animation="fadeInLeft">
                {/* Logo section*/}
                <Image style={styles.img}
                                  source={require('../images/logo.png')}
                />
                <Text style={styles.header}
                >
                    Food Delivery
                </Text>

                {/* Input section*/}
                <TextInput style={styles.txtInput}
                           placeholder={string('strEmail')}
                           underlineColorAndroid={'transparent'}
                           autoCapitalize = "none"
                           onChangeText = {this.handleEmail}/>
                <TextInput style={styles.txtInput}
                           placeholder={string('strPassword')}
                           secureTextEntry={true}
                           underlineColorAndroid={'transparent'}
                           autoCapitalize = "none"
                           onChangeText = {this.handlePassword}/>
                {/* Button section*/}
                <TouchableOpacity style={styles.btnLink} onPress={() => Actions.forget()} >
                    <Text style={styles.txtLink}>{string('strForget')}</Text>
                </TouchableOpacity>
                <ButtonStandard text={string('strLogin')} onPress={() => { this.login(this.state.email, this.state.password);  }} />
                <ButtonStandard text={string('strSignUp')} onPress={() => Actions.register()} />
                <View style={styles.languageContainer}>
                    <TouchableOpacity style={styles.btnLanguage}
                                      onPress={() => changeLanguage('en',this)}
                    >
                        <Image style={styles.imgLanguage}
                               source={require('../images/english.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnLanguage}
                                      onPress={() => changeLanguage('vi',this)}
                    >
                        <Image style={styles.imgLanguage}
                               source={require('../images/vietnamese.png')}
                        />
                    </TouchableOpacity>
                </View>
            </Animatable.View>
        );
    }
}
const mapStateToProps = state => ({
    isAuthenticated: state.authenticationReducer.isAuthenticated,
    accessToken: state.authenticationReducer.accessToken,
    userInfo: state.authenticationReducer.userInfo,
});

const mapDispatchToProps = dispatch => ({
    authenticateF: (data) => {
        dispatch(authenticate(data));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);

// define styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: 60,
        paddingLeft: 60,
    },
        img: {
            width: 160,
            height: 160,
            marginTop: 20
        },
        header: {
            fontSize: 24,
            fontWeight: 'bold',
            color: '#3399ff',
            marginTop: 10,
            textAlign: 'center',

        },
        txtInput: {
            alignSelf: 'stretch',
            height: 40,
            marginTop: 20,
            color: '#000000',
            backgroundColor: 'white',
            borderColor: 'lightgrey',
            borderWidth: 1,
            elevation: 2

        },
        btnLink: {
            alignSelf: 'stretch',
            alignItems: 'center',
            padding: 10,
            marginTop: 20
        },
        txtLink: {
            color: '#3399ff',
        },
        languageContainer: {
            flexDirection: 'row',
            marginTop: 10,
            alignItems: 'center',
            justifyContent: 'space-between',
        },
            btnLanguage: {
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            },
            imgLanguage: {
                width: 60,
                height: 30
            }
});