// import libraries
import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux';
import {
    View,
    ScrollView,
    Text,
    TextInput,
    Image,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    Alert,
    Picker,
    Modal
} from 'react-native';
import ButtonStandard from '../ButtonStandard';
import Icon from 'react-native-vector-icons/FontAwesome';
import {getUserInfo, logOut, updateUserInfo} from "./AuthenticationAction";
import { connect } from 'react-redux';
import { string } from "../../localization/i18n";

type Props = {};

class Account extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            content: [],
            district: [],
            districtSelected: '',
            ward: [],
            wardSelected: '',
            modalVisible: false,
            isLoading: true,
            isEditPhone: false,
            isEditAddress: false,
        };
    };
    getDistrict = () => {
        try {
            fetch('https://food-delivery-server.herokuapp.com/district/getAll', {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log(responseJson);
                    this.setState({
                        district: responseJson,
                    });
                })
                .catch((error) => {
                    console.error(error);
                });
        } catch (error) {
            console.log('error');
        }
    };
    // Handle info changes
    handlePhone = (text) => {
        this.setState({ phone: text })
    };
    handleStreet = (text) => {
        this.setState({ street: text })
    };
    handleDistrict = (idDistrict) => {
        this.setState({
            districtSelected: idDistrict
        });
        console.log(idDistrict);
        try {
            fetch('https://food-delivery-server.herokuapp.com/ward/getAllByDistrict?id=' + idDistrict, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log(responseJson);
                    this.setState({
                        ward: responseJson,
                    });
                })
                .catch((error) => {
                    console.error(error);
                });
        } catch (error) {
            console.log('error');
        }

    };
    handleWard = (idWard) => {
        this.setState({
            wardSelected: idWard
        });
        console.log(idWard);
    };
    // User info functions
    updateInfo = (phone, street, district, ward, email) => {
        const { updateInfoF } = this.props;
        // Get names of district and ward based on ID
        var districtName = this.state.district.find(data => data.id === district).name;
        console.log(districtName);
        var wardName = this.state.ward.find(data => data.id === ward).name;
        console.log(wardName);
        // Create address
        var address = street + ', ' + wardName + ', ' + districtName;
        // Create info data to update
        var data = {
            phone: phone,
            idDistrict: district,
            idWard: ward,
            street: street,
            email: email,
            address: address
        };
        console.log(data);
        updateInfoF(
            this.props.accessToken,
            JSON.stringify(data)
        );
    };
    //
    logOut = () => {
        const { logOutF } = this.props;
        logOutF();
        console.log(this.props.accessToken);
        Actions.login();
    };

    //
    componentWillReceiveProps(props)
    {
        const { isGotInfo } = props;
        console.log('isGotInfo: ', isGotInfo);

        if( isGotInfo )
        {
            this.setState({
                isLoading: false
            });
        }
    };

    componentWillMount = () => {
        this.getDistrict();
        if ( this.props.isGotInfo )
        {
            this.setState({
                isLoading: false
            });
        }
        console.log(this.props.isGotInfo);
        console.log(this.props.userInfo);
    };
    componentDidMount = () => {

    };
    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    };
    renderDistrictList() {
        return this.state.district.map(district => {
            return (
                <Picker.Item key={district.id} value={district.id} label={district.name}/>
            );
        })
    };
    renderWardList() {
        return this.state.ward.map(ward => {
            return (
                <Picker.Item key={ward.id} value={ward.id} label={ward.name}/>
            );
        })
    };
    render() {
        if (this.state.isLoading) {
            return (
                    <ActivityIndicator size="large" color={"#3399ff"} />
            );
        }
        if (!this.state.isLoading) {
            return (
                <ScrollView style={styles.container}>
                    <View style={styles.userNameContainer}>
                        <Text style={styles.lblEmail}>
                            { this.props.userInfo.email }
                        </Text>
                    </View>

                    <View style={styles.userInfoContainer}>
                        <View style={styles.infoContainer}>
                            <Icon style={styles.iconInfo} name="phone" size={20} color="#000000"/>
                                <Text style={styles.lblInfo}>
                                    { this.props.userInfo.phone }
                                </Text>
                        </View>

                        <View style={styles.infoContainer}>
                            <Icon style={styles.iconInfo} name="home" size={20} color="#000000"/>
                            <Text style={styles.lblInfo}>
                                { this.props.userInfo.address }
                            </Text>
                        </View>

                        <View style={styles.infoContainer}>
                            <Icon style={styles.iconInfo} name="envelope" size={20} color="#000000"/>
                            <Text style={styles.lblInfo}>
                                { this.props.userInfo.email }
                            </Text>
                        </View>
                        <ButtonStandard
                            text={string('strChangeInfo')}
                            onPress={() => {
                                this.setModalVisible(true);
                            }}
                        />
                        <ButtonStandard
                            text={string('strSignOut')}
                            onPress={() => {
                                this.logOut();
                            }}
                        />


                    </View>

                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {console.log('Close Modal')}}
                    >
                        <View style={styles.modalChangeInfo}>
                            <View>

                                <TextInput style={styles.txtInput}
                                           placeholder={string('strPhone')}
                                           underlineColorAndroid={'transparent'}
                                           autoCapitalize = "none"
                                           onChangeText = {this.handlePhone}/>
                                <TextInput style={styles.txtInput}
                                           placeholder={string('strStreet')}
                                           underlineColorAndroid={'transparent'}
                                           autoCapitalize = "none"
                                           onChangeText = {this.handleStreet}/>
                                {/* Render district and ward */}
                                <Picker selectedValue={this.state.districtSelected}
                                        style={styles.picker}
                                        onValueChange={(itemValue) => this.handleDistrict(itemValue)}>
                                    {this.renderDistrictList()}
                                </Picker>
                                <Picker selectedValue={this.state.wardSelected}
                                        style={styles.picker}
                                        onValueChange={(itemValue) => this.handleWard(itemValue)}>
                                    {this.renderWardList()}
                                </Picker>
                                {/* */}
                                <ButtonStandard
                                    text={string('strUpdate')}
                                    onPress={() => {
                                        this.updateInfo(this.state.phone, this.state.street,
                                                        this.state.districtSelected, this.state.wardSelected,
                                                        this.props.userInfo.email);
                                    }}
                                />
                                <ButtonStandard
                                    text={string('strClose')}
                                    onPress={() => {
                                        this.setModalVisible(!this.state.modalVisible);
                                    }}
                                />
                            </View>
                        </View>
                    </Modal>
                </ScrollView>
            );
        }
    }
}
const mapStateToProps = state => ({
    isGotInfo: state.authenticationReducer.isGotInfo,
    userInfo: state.authenticationReducer.userInfo,
    accessToken: state.authenticationReducer.accessToken,
});

const mapDispatchToProps = dispatch => ({
    getInfoF: (token) => {
        dispatch(getUserInfo(token));
    },
    updateInfoF: (token, data) => {
        dispatch(updateUserInfo(token, data));
    },
    logOutF: () => {
        dispatch(logOut());
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Account);
// define styles
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        paddingLeft: 20,
        paddingRight: 20
    },
    userNameContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 200
    },
        lblEmail: {
            fontWeight: 'bold',
            fontSize: 20
        },
    userInfoContainer: {
        height: 300
    },
        infoContainer: {
            height: 50,
            flex: 1,
            flexDirection: 'row',
            backgroundColor: 'white',
            alignItems: 'center',
            borderRadius: 5,
            elevation: 3,
            marginBottom: 10
        },
            iconInfo: {
                flex: 2,
                paddingLeft: 20
            },
            lblInfo: {
                flex: 8,
                fontSize: 12
            },
    modalChangeInfo: {
        paddingLeft: 20,
        paddingRight: 20,
    },
    txtInput: {
        alignSelf: 'stretch',
        height: 40,
        marginTop: 20,
        color: '#000000',
        backgroundColor: 'white',
        borderColor: 'lightgrey',
        borderWidth: 1,
        elevation: 2

    },
    picker: {
        alignSelf: 'stretch',
        height: 50,
        marginTop: 20
    }
});