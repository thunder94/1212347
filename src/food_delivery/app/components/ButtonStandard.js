import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

class ButtonStandard extends Component {
    render() {
        const { text, onPress} = this.props;
        return (
            <TouchableOpacity
                style={styles.buttonStyle}
                onPress={() => onPress()}
            >
                <Text style={styles.textStyle}>{text}</Text>
            </TouchableOpacity>
        );
    }
}

ButtonStandard.propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    textStyle: {
        color: '#ffff',
        fontWeight: 'bold'
    },

    buttonStyle: {
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 10,
        borderRadius: 10,
        backgroundColor: '#3399ff',
        marginTop: 20,
    }
});

export default ButtonStandard;