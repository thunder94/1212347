import {combineReducers} from 'redux';
import { persistReducer } from 'redux-persist'
import authenticationReducer from '../components/authentication/AuthenticationReducer';
import merchantReducer from '../components/merchant/MerchantReducer';
import orderReducer from '../components/order/OrderReducer';
import thunk from 'redux-thunk';
import storage from 'redux-persist/lib/storage';

const authenticationConfig = {
    key: 'authentication',
    storage,
    blacklist: ['isAuthenticating', 'isGotInfo', 'userInfo']
}
const merchantConfig = {
    key: 'merchant',
    storage,
    blacklist: ['isGotDetail', 'merchantDetail', 'cartList']
}
const orderConfig = {
    key: 'order',
    storage,
    blacklist: ['isGotOrderDetail', 'orderDetail']
}
const rootReducer = combineReducers({
    authenticationReducer: persistReducer(authenticationConfig, authenticationReducer),
    merchantReducer: persistReducer(merchantConfig, merchantReducer),
    orderReducer: persistReducer(orderConfig, orderReducer),

})

export default rootReducer;