import I18n from 'react-native-i18n';
import { en } from './en';
import { vi } from './vi';

I18n.fallbacks = true;

I18n.translations = {
    en: en,
    vi: vi
};

export const string = (str) => {
    return I18n.t(str);
};

export const changeLanguage = (lang, component) => {
    I18n.locale = lang;
    component.forceUpdate();
};