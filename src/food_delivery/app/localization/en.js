export const en = {
    // Authentication
    strLogin: 'Login',
    strSignUp: 'Sign Up',
    strForget: 'Forget Password?',
    strForgetContent: 'Enter your email so we can send you a link to reset your password',
    strResetPassword: 'Reset Password',
    strCreateAccount: 'Create Account',
    strEmail: 'Email',
    strPassword: 'Password',
    // Merchant
    headerMerchantSearch: 'SEARCH MERCHANT',
    headerMerchantNearby: 'MERCHANT NEARBY',
    strMerchantSearchText: 'Search merchants by name...',
    // Basket
    headerBasket: 'BASKET',
    strImage: 'Image',
    strFoodName: 'Food Name',
    strNumber: 'Number',
    strPurchase: 'Purchase',
    // Order
    headerOrderHistory: 'ORDER HISTORY',
    strOrder: 'Order',
    strCreated: 'Date Created',
    strCompleted: 'Date Completed',
    strTotalPrice: 'Total Price',
    strStatus: 'Status',
    strCancel: 'Cancel',
    strReorder: 'Reorder',
    // Account
    strChangeInfo: 'Change Information',
    strSignOut: 'Sign Out',
    strPhone: 'Phone',
    strStreet: 'Street',
    strUpdate: 'Update',
    strClose: 'Close'
};