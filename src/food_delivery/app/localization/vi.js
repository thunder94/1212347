export const vi = {
    // Authentication
    strLogin: 'Đăng Nhập',
    strSignUp: 'Đăng Ký',
    strForget: 'Quên Mật Khẩu?',
    strForgetContent: 'Nhập email của bạn để chúng tôi có thể gửi đường dẫn phục hồi mật khẩu',
    strResetPassword: 'Khôi Phục Mật Khẩu',
    strCreateAccount: 'Tạo Tài Khoản',
    strEmail: 'Email',
    strPassword: 'Mật Khẩu',
    // Merchant
    headerMerchantSearch: 'TÌM KIẾM QUÁN ĂN',
    headerMerchantNearby: 'QUÁN ĂN GẦN ĐÂY',
    strMerchantSearchText: 'Nhập tên quán ăn cần tìm...',
    // Basket
    headerBasket: 'GIỎ HÀNG',
    strImage: 'Ảnh',
    strFoodName: 'Món Ăn',
    strNumber: 'Số Lượng',
    strPurchase: 'Đặt Món',
    // Order
    headerOrderHistory: 'LỊCH SỬ ĐƠN HÀNG',
    strOrder: 'Đơn Hàng',
    strCreated: 'Ngày Tạo',
    strCompleted: 'Ngày Giao',
    strTotalPrice: 'Tổng Cộng',
    strStatus: 'Trạng Thái',
    strCancel: 'Hủy',
    strReorder: 'Đặt Lại',
    // Account
    strChangeInfo: 'Thay Đổi Thông Tin',
    strSignOut: 'Đăng Xuất',
    strPhone: 'Số Điện Thoại',
    strStreet: 'Số Nhà, Đường',
    strUpdate: 'Cập Nhật',
    strClose: 'Đóng'
};